const main = async () => {
    document
        .querySelector(".installButton")
        .addEventListener("click", createAndInstallSW);
    document
        .querySelector(".uninstallButton")
        .addEventListener("click", uninstallSW);
};

const createAndInstallSW = async () => {
    await createSW();
    await installSW();
};

const createSW = async () => {
    const swFileResp = await fetch("./sw.js");
    const swFileData = await swFileResp.text();
    await fetch("/save", {
        headers: {
            "Content-Type": "application/json",
        },
        credentials: "include",
        referrer: `${window.location.origin}/edit/sw.js`,
        body: JSON.stringify({
            text: swFileData,
            errors: [],
            timestamp: Math.round(new Date().getTime() / 1000),
        }),
        method: "POST",
        mode: "cors",
    });
};

const installSW = async () => {
    console.log("Installing SW");

    try {
        const registration = await window.navigator.serviceWorker.register(
            "/sw.js",
            { scope: "/" }
        );
        console.log("SW Registered, registration: ", registration);
        alert("SW Registered");
    } catch (err) {
        console.error("SW registration failed", err);
        alert("SW registration failed (check console for err)");
    }
};

const uninstallSW = async () => {
    console.log("Uninstalling");
    try {
        const registration = await window.navigator.serviceWorker.getRegistration(
            "/sw.js"
        );
        registration.unregister();
        console.log("SW Unregistered, registration: ", registration);
        alert("SW Unregistered");
    } catch (err) {
        console.error("SW unregistration failed", err);
        alert("SW unregistration failed (check console for err)");
    }
};

main().then();
