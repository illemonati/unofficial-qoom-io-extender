# Unofficial Qoom IO Extender

Extends qoom front end plans with custom js and css to run within the coding space

1. Copy this folder into the root of your coding space (via any method: eg. git integration, folder upload, etc)
2. Make sure that you copied this FOLDER itself and not the contents of this folder
3. Navigate to index.html in this folder in your coding space (should be at "https://(BASE_URL_OF_YOUR_CODING_SPACE)/unofficial-qoom-io-extender/index.html")
4. Follow the instructions on the screen

## Instruction Video

[![Instructional Video Link](http://img.youtube.com/vi/bcCDTG5O7tQ/0.jpg)](https://youtu.be/bcCDTG5O7tQ "Instructional Video")
