self.addEventListener("install", (event) => {
    self.skipWaiting();
});

const handleNetwork = async (event) => {
    // Check Qoom CSS
    if (event.request.url.endsWith("qoom.css")) {
        const ogQoomCssResp = await fetch(event.request);
        const ogQoomCss = await ogQoomCssResp.text();
        const newCss = `
                @import url("${self.location.protocol}//${self.location.host}/unofficial-qoom-io-extender/custom/css/index.css");
                ${ogQoomCss}
            `;
        return new Response(newCss, {
            status: ogQoomCssResp.status,
            statusText: ogQoomCssResp.statusText + " + Injected CSS",
            headers: ogQoomCssResp.headers,
        });
    }

    const normalResponse = await fetch(event.request);
    // Check if html
    if (
        normalResponse.headers.get("Content-Type") &&
        normalResponse.headers.get("Content-Type").includes("text/html")
    ) {
        let body = await normalResponse.text();
        const src = "/unofficial-qoom-io-extender/custom/js/index.js";
        body += `
                <script src="${src}" defer type="module"></script>
            `;
        return new Response(body, {
            status: normalResponse.status,
            statusText: normalResponse.statusText + " + Injected Html",
            headers: normalResponse.headers,
        });
    }
    return normalResponse;
};

self.addEventListener("fetch", (event) => {
    event.respondWith(handleNetwork(event));
});
