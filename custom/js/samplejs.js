const logInstalledMessage = () => {
    console.log("You have Unofficial Qoom IO Extender Installed");
};

const addToFooter = () => {
    const footer = document.querySelector("footer");
    if (!footer) return;
    const copyrightContainer = footer.querySelector(".copyright");
    if (!copyrightContainer) return;
    copyrightContainer.innerText += " (Unofficial Qoom IO Extender Installed)";
};

export { logInstalledMessage, addToFooter };
